import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'src/app/models/user';
import { Userservice } from '../../services/user.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  providers: [Userservice]
})
export class RegisterComponent {
  title: string;
  user: User;
  status: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private userService: Userservice

  ) {
    this.title = 'Registro';
    this.user = new User(1, 'ROLE_USER', '', '', '', '');
  }

  ngOnInit() {
    console.log('register.component cargado correctamente!!')
  }

  onSubmit() {
    console.log(this.user);
    /*console.log(this.userService.pruebas()) */

    this.userService.register(this.user).subscribe(
      response => {
        console.log(response);
        if (response.status == 'success') {
          this.status = response.status;
          //vaciar el formulario
          this.user = new User(1, 'ROLE_USER', '', '', '', '');
          //Form_register.reset();

        }else{
          this.status = 'error';
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
}